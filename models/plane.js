//developer - Vyshnavi Yalamareddy -->
const mongoose = require('mongoose')

const PlaneSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  PlaneNumber: {
    type: String,
    required: true,
    default: 'N98488'
  },
  NoofSeats: {
    type: Number,
    required: false,
    default:4
  },
  TotalCapacity: {
    type: Number,
    required: false,
    default: 50
  }
})
module.exports = mongoose.model('Plane', PlaneSchema)
